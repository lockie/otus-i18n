* Интернационализация приложений на C

** Интернационализация (internationalization, i18n)
** Локализация (localization, l10n)
** Хранение текста в памяти компьютера
   Абсолютный минимум, который каждый разработчик должен знать о Unicode и кодировках: <https://cyberforum.ru/blogs/33029/blog5139.html>

*** ASCII <https://asciitable.com>
*** Кодовые страницы: ISO, CP, KOI8
    [[file:craco.png]]
*** Заблуждение: Unicode это просто 16-битные символы
*** Code points: U+0041 = A
*** Кодировки Unicode: UTF-8, UTF-16, USC-4, BOM
    UTF-8 изобретён Кеном Томпсоном и Робом Пайком
*** Нет смысла держать строку, не зная, в какой она кодировке
*** [[https://unicode-org.github.io/icu-docs/apidoc/released/icu4c][libicu]]
*** [[https://developer-old.gnome.org/glib/unstable/glib-Unicode-Manipulation.html][GLib]]
** gettext
   <https://gnu.org/software/gettext>

   [[file:gettext.png]]

*** [[https://www.gnu.org/software/gettext/manual/html_node/List-of-Programming-Languages.html][Существуют реализации]] для:
    C, C++, Objective-C, Java, JavaScript, Python, Ruby, Perl, PHP, Bash, Scala, Clojure, R, Tcl, FreePascal, Common Lisp, а так же фреймворков wxWidgets, Qt и Mono.
*** Переводимые строки обозначаются своими английскими оригиналами
#+BEGIN_SRC C
  #include <libintl.h>

  printf(gettext("Hello! My name is %s.\n"), name);

  #define _(s) gettext(s)
  printf(_("Hello! My name is %s.\n"), name);
#+END_SRC

*** Антипаттерны
#+BEGIN_SRC C
  // BAD
  printf(gettext ("%s is full"),
         capacity > 5000000 ? gettext ("disk") : gettext ("floppy disk"));
  // GOOD
  printf(capacity > 5000000 ?
         gettext ("disk is full")
         : gettext ("floppy disk is full"));

  // BAD
  printf("Implicit rule search has%s been done.\n",
         f->tried_implicit ? "" : " not");
  // GOOD
  printf(f->tried_implicit
         ? "Implicit rule search has been done.\n",
         : "Implicit rule search has not been done.\n");

  // BAD
  printf(gettext ("%d file%s processed"), nfiles,
         nfiles != 1 ? "s" : "");
  // GOOD
  printf(ngettext ("%d files processed", "%d file processed", nfiles),
         nfiles);
#+END_SRC

*** ngettext
    [[https://gnu.org/software/gettext/manual/html_node/Plural-forms.html][Additional functions for plural forms]]

*** [[https://poedit.net][Poedit]]
    [[file:poedit.png]]

*** [[https://wiki.gnome.org/Apps/Gtranslator][Gtranslator]]
*** [[https://userbase.kde.org/Lokalize][Lokalize]]
*** Использование переведённых строк
#+BEGIN_SRC C
  int main()
  {
      setlocale(LC_ALL, "");
      bindtextdomain("hello", "/usr/share/locale/");
      textdomain("hello");

      /* ... */
#+END_SRC

*** Локаль
[[https://gnu.org/software/libc/manual/html_node/Locales.html][Locales - GNU C Library]]
[[https://en.cppreference.com/w/c/locale/setlocale][setlocale]], [[https://www.gnu.org/software/gettext/manual/html_node/Locale-Environment-Variables.html][LANGUAGE/LANG]], [[https://man7.org/linux/man-pages/man1/locale.1.html][locale(1)]]

- ""
- =C=, =POSIX=
- =language[_territory][.codeset][@modifier]=

Пример: =de_AT.UTF8@euro=

*** Категории (аспекты)
- ~LC_CTYPE~ ([[https://en.cppreference.com/w/c/string/byte/islower][islower]] и т.д.)
- ~LC_NUMERIC~ ([[https://en.cppreference.com/w/c/io/fprintf][printf]], [[https://en.cppreference.com/w/c/string/byte/atof][atof]] и т.д.)
- ~LC_TIME~ ([[https://en.cppreference.com/w/c/chrono/strftime][strftime]] и т.д.)
- ~LC_COLLATE~ ([[https://en.cppreference.com/w/c/string/byte/strcoll][strcoll]] и т.д.)
- ~LC_MONETARY~ ([[https://man7.org/linux/man-pages/man3/strfmon.3.html][strfmon]])
- ~LC_MESSAGES~ (gettext)

*** Parsing JSON: locale-dependent bug
    <https://git.io/Jte2C>
*** [[https://fedoraproject.org/wiki/How_to_do_I18N_through_gettext][How to do I18N through gettext]]
*** Пример из реальности: [[https://gitlab.com/muttmua/mutt][mutt]]
*** CMake: [[https://cmake.org/cmake/help/latest/module/FindGettext.html][FindGettext]]
** [[https://otus.pw/Q16T][Опрос]]
