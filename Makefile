
TEXTDOMAIN = helloworld

MSGMERGE  ?= msgmerge
MSGFMT    ?= msgfmt
XGETTEXT  ?= xgettext

SOURCES = $(wildcard src/*.c)
LOCALES = $(wildcard locale/*/LC_MESSAGES/$(TEXTDOMAIN).po)

all: hello

hello: $(SOURCES)
	$(CC) $^ -o $@ -Wall -Wextra -Wpedantic -std=c2x

pot: locale/$(TEXTDOMAIN).pot

%.pot: $(SOURCES)
	$(XGETTEXT) -d $(TEXTDOMAIN) -o $@ -k_ -k"_n:1,2" -s $(SOURCES)

translation: $(subst .po,.mo,$(LOCALES))

update-translation: $(LOCALES)
	$(MAKE) pot

%.po: locale/$(TEXTDOMAIN).pot
	$(MSGMERGE) --update $@ $^
	touch $@  # HACK

%.mo: %.po
	$(MSGFMT) $^ -o $@

clean:
	rm -f hello core $(subst .po,.mo,$(LOCALES))

.PHONY: all pot translation update-translation clean
