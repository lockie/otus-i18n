#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <libintl.h>


#define _(s) gettext(s)
#define _n(s, p, n) ngettext(s, p, n)

#define LOCALEDIR "locale"

int main(int argc, char**)
{
    setlocale(LC_ALL, "");
    bindtextdomain("helloworld", LOCALEDIR);
    textdomain("helloworld");

    printf(_("Hello World\n"));

    int nparams = argc - 1;
    printf(_n("Processed %d parameter\n",
              "Processed %d parameters\n",
              nparams),
           nparams);
    return EXIT_SUCCESS;
}
